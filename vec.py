# Program na derivování jednoduchých výrazů jedné proměnné.
# Autor: Tomáš Garaj

import sys
import logging

class Clen:
    def __init__(self, koeficient, mocnina, funkce, argument):
        self.koeficient = koeficient
        self.mocnina = mocnina
        self.funkce = funkce
        self.argument = argument


zname_derivace = {'sin': 'cos', 'cos': '-sin', 'tan': 'cos^(-2)', 'cotan': '-sin^(-2)', 'exp': 'exp', 'log': '1/'}

def over_zavorky(vyraz):

    zavorky = 0
    for i in vyraz:
        if i == ')':
            zavorky += 1
        elif i == '(':
            zavorky += -1
    if zavorky != 0:
        return False


def zderivuj(vyraz):
    cleny, znamenka = rozdel_podle_plusu(vyraz)
    vysledek = []
    for clen in cleny:
        cast1 = ''
        cast2 = ''
        zavorky = 0
        i = 0
        while i != len(clen):
            if clen[i] == '(':
                zavorky += -1
                cast1 += clen[i]
            elif clen[i] == ')':
                zavorky += 1
                cast1 += clen[i]
            elif clen[i] == '*' and zavorky == 0:
                if clen[i-1].isnumeric() is False and clen[i+1].isnumeric() is False:
                    cast2 = clen[i+1:]
                    break
                else:
                    cast1 += clen[i]
            elif clen[i] == '/' and zavorky == 0:
                cast2 = clen[i + 1:]
                break
            else:
                cast1 += clen[i]
            i += 1
        if i != len(clen):
            cast2 = clen[i+1:]
            if clen[i] == '*':
                derivace = zderivuj_soucin(cast1, cast2)
            else:
                derivace = zderivuj_podil(cast1, cast2)
        else:
            derivace = zderivuj_clen(cast1)
        vysledek.append(derivace)
    vysledna_derivace = ''
    vysledna_derivace += vysledek[0]
    if znamenka:
        for i in range(len(znamenka)):
            if vysledek[i + 1] != '0':
                if vysledek[i + 1][0] == '-':
                    if znamenka[i] == '+':
                        vysledna_derivace += '-'
                        vysledna_derivace += vysledek[i + 1][1:]
                    else:
                        vysledna_derivace += '+'
                        vysledna_derivace += vysledek[i + 1][1:]
                else:
                    vysledna_derivace += znamenka[i]
                    vysledna_derivace += vysledek[i + 1]
    return vysledna_derivace


def zderivuj_clen(clen):

    vstup = zpracuj_clen(clen + '@')
    if vstup.funkce == '' and vstup.argument == '':
        return '0'
    elif vstup.funkce == '' and vstup.argument == 'x':
        if vstup.mocnina == '':
            return vstup.koeficient
        else:
            if int(vstup.mocnina) - 1 != 1:
                return str(int(vstup.mocnina) * int(vstup.koeficient)) + '*x^' + str(int(vstup.mocnina) - 1)
            else:
                return str(int(vstup.mocnina) * int(vstup.koeficient)) + '*x'
    elif vstup.funkce in zname_derivace:
        if zderivuj(vstup.argument) == '0':
            return '0'
        else:
            vysledek = ''
            if vstup.mocnina != '':
                vysledek += str(int(vstup.mocnina) * int(vstup.koeficient)) + '*' + vstup.funkce + '(' + vstup.argument + ')'
                if int(vstup.mocnina) - 1 != 1:
                    vysledek += '^' + str(int(vstup.mocnina) - 1)
                vysledek += '*'
            else:
                if vstup.koeficient != '1':
                    vysledek += vstup.koeficient + '*'
            vysledek += zname_derivace[vstup.funkce] + '(' + vstup.argument + ')'
            if zderivuj(vstup.argument) != '1':
                derivace_argumentu = zderivuj(vstup.argument)
                vysledek += '*' + '(' + derivace_argumentu + ')'
        return vysledek
    else:
        vysledek = ''
        if vstup.mocnina != '':
            vysledek += str(int(vstup.mocnina) * int(vstup.koeficient)) + '*(' + vstup.argument + ')'
            if int(vstup.mocnina) - 1 != 1:
                vysledek += '^' + str(int(vstup.mocnina) - 1)
        else:
            if vstup.koeficient != '1':
                vysledek += vstup.koeficient + '*'
            vysledek += zderivuj(vstup.argument)
        if zderivuj(vstup.argument) != '1':
            vysledek += '*' + zderivuj(vstup.argument)
        return vysledek


def zpracuj_clen(vstup):

    vystup = Clen('1', '', '', '')
    i = 0
    while vstup[i] != '@':
        if vstup[i] == '(':
            zavorky = -1
            i += 1
            while zavorky != 0:
                if vstup[i] == '(':
                    zavorky += -1
                elif vstup[i] == ')':
                    zavorky += 1
                    if zavorky == 0:
                        i += 1
                        break
                vystup.argument += vstup[i]
                i += 1
        elif vstup[i] == 'x':
            vystup.argument = 'x'
            i += 1
        elif vstup[i] == '^':
            i += 1
            while vstup[i].isnumeric() is True:
                vystup.mocnina += vstup[i]
                i += 1
        elif vstup[i] in ['s', 'c', 't', 'e', 'l']:
            while vstup[i] != '(':
                vystup.funkce += vstup[i]
                i += 1
        elif vstup[i].isnumeric() is True:
            koeficient = ''
            while vstup[i].isnumeric() is True:
                koeficient += vstup[i]
                i += 1
            vystup.koeficient = str(int(vystup.koeficient)*int(koeficient))
        else:
            if vstup[i] != '@':
                i += 1
    return vystup


def rozdel_podle_plusu(string):

    string = string.replace(' ', '')
    vyrazy_k_derivovani = []
    znamenka = []
    stavajici_vyraz = ''
    zavorky = 0
    for i in range(len(string)):
        if (string[i] == '+' or string[i] == '-') and zavorky == 0:
            znamenka.append(string[i])
            vyrazy_k_derivovani.append(stavajici_vyraz)
            stavajici_vyraz = ''
        else:
            stavajici_vyraz += string[i]
            if string[i] == '(':
                zavorky += -1
            elif string[i] == ')':
                zavorky += 1
    vyrazy_k_derivovani.append(stavajici_vyraz)
    return vyrazy_k_derivovani, znamenka


def zderivuj_soucin(cast1, cast2):

    a = zderivuj_clen(cast1)
    b = zderivuj(cast2)
    if a == '0':
        c = '0'
    elif a == '1':
        c = cast2
    else:
        c = a + '*' + cast2
    if b == '0':
        d = '0'
    elif b == '1':
        d = cast1
    else:
        d = b + '*' + cast1
    if c == '0' and d == '0':
        return '0'
    elif c == '0':
        return d
    elif d == '0':
        return c
    else:
        if d[0] == '-':
            derivace = c + d
        else:
            derivace = c + '+' + d
        return derivace


def zderivuj_podil(cast1, cast2):

    a = zderivuj_clen(cast1)
    b = zderivuj(cast2)
    if a == '0':
        c = '0'
    elif a == '1':
        c = cast2
    else:
        c = a + '*' + cast2
    if b == '0':
        d = '0'
    elif b == '1':
        d = cast1
    else:
        d = b + '*' + cast1
    if c == '0' and d == '0':
        return '0'
    elif c == '0':
        if d[0] == '-':
            return d + '/(' + cast2 + ')^2'
        else:
            return '-' + d + '/(' + cast2 + ')^2'
    elif d == '0':
        return c + '/(' + cast2 + ')^2'
    else:
        if d[0] == '-':
            derivace = '(' + c + '+' + d + ')'
        else:
            derivace = '(' + c + '-' + d + ')'
        derivace += '/(' + cast2 + ')^2'
        return derivace

if __name__ == '__main__':
    logger = logging.getLogger('')
    logger.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(levelname)s - %(message)s")
    file_handler = logging.FileHandler('tomasuv_logger.log')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    for line in sys.stdin:
        try:
            if over_zavorky(line) == False:
                logger.error(f'User input: {line}Result: Mismatched parentheses')
                continue
            line_output = zderivuj(line)
            logger.info(f'User input: {line}Result: {line_output}\n')
        except Exception as e:
            logger.error(f'User input: {line}Result: {e}')


